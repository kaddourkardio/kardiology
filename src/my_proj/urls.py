"""
with the new version of django 2.0 or higher, urls.py face the biggest rework
"""
from django.conf.urls import include, url
from django.urls import path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
# import des fichier locaux urls.py
import profiles.urls
import clinic.urls
import courrier.urls
import accounts.urls
from . import views
# from clinic.views import current_admission

urlpatterns = [
    url(r'^accueil/$', views.HomePage.as_view(),
        name='home'),
    url(r'^about/$', views.AboutPage.as_view(),
        name='about'),
    url(r'^users/', include(profiles.urls,
                            namespace='profiles')),
    path(r'admin/', admin.site.urls),
    url(r'^me/', include(accounts.urls, namespace='accounts')),
    url(r'^clinic/', include(clinic.urls, namespace='clinics')),
    url(r'^courrier/', include(courrier.urls, namespace='courriers'))
]

# User-uploaded files like profile pics need to be served in development
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Include django debug toolbar if DEBUG is on
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
