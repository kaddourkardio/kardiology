from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings
from .views import (ListCoronarographies, CreateCoronarographie,
                    UpdateCoronarographie, DetailCoronarographie,
                    UpdateCoronarographie, ListStimulations,
                    DetailStimulation, CreateStimulation,
                    UpdateStimulation, entry_as_pdf, stimulation_pdf)

from . import models

urlpatterns = [
    url(r'^coronarographies/$', ListCoronarographies.as_view(),
        name='list_coronarographies'),
    url(r'^coronarographie/(?P<pk>\d+)/edit/$', UpdateCoronarographie.as_view(),
        name='update_coronarographie'),
    url(r'^coronarographie/(?P<pk>\d+)/$', DetailCoronarographie.as_view(),
        name='detail_coronarographie'),
    url(r'^coronarographie/new/$', CreateCoronarographie.as_view(),
        name='create_coronarographie'),
    url(r'^stimulations/$', ListStimulations.as_view(),
        name='list_stimulations'),
    url(r'^stimulation/(?P<pk>\d+)/edit/$', UpdateStimulation.as_view(),
        name='update_stimulation'),
    url(r'^stimulation/(?P<pk>\d+)/$', DetailStimulation.as_view(),
        name='detail_stimulation'),
    url(r'^coronarographie/patient(?P<pk1>\d+)_coro(?P<pk2>\d+)\.pdf$',
        entry_as_pdf, name='rapport_coro'),
    url(r'stimulation/patient(?P<pk1>\d+)_pace(?P<pk2>\d+)\.pdf$',
        stimulation_pdf, name='stimulation_pdf'),
    url(r'^stimulation/new/$', CreateStimulation.as_view(),
        name='create_stimulation'),
]
