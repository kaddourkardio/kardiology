# modelform pour me faciliter la vie
from django.forms import ModelForm
from intervention.models import Coronarographie, Stimulation

class CoronarographieForm(ModelForm):

    class Meta:
        model = Coronarographie
        fields = '__all__'
form1 = CoronarographieForm

class StimulationForm(ModelForm):

    class Meta:
        model = Stimulation
        fields = '__all__'
form2 = StimulationForm
