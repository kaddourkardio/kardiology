from django.conf.urls import url, include
from django.conf import settings
from . import views

app_name = 'courrier'
urlpatterns = [
    url(r'patient(?P<pk1>\d+)-courrier(?P<pk2>\d+)\.mkiv$',
        views.courrier_mkiv, name='courrier_mkiv'),
    url(r'patient(?P<pk1>\d+)-stomato(?P<pk2>\d+)\.pdf$',
        views.stomato_pdf, name='stomato_pdf'),
    url(r'patient(?P<pk1>\d+)-certificat(?P<pk2>\d+)\.pdf$',
        views.certificat_pdf, name='certificat_pdf'),
    url(r'patient(?P<pk1>\d+)-lettre(?P<pk2>\d+)\.pdf$',
        views.courrier_pdf, name='courrier_pdf'),
    url(r'patient(?P<pk1>\d+)-ordonnance(?P<pk2>\d+)\.pdf$',
        views.ordonnance_pdf, name='ordonnance_pdf'),
    url(r'patient(?P<pk1>\d+)-arret(?P<pk2>\d+)\.pdf$',
        views.arret_pdf, name='arret_pdf'),
]
