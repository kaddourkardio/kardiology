# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-03 18:28
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('clinic', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Arret',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('arret_date', models.DateField(default=datetime.datetime(2017, 8, 3, 18, 28, 17, 877249, tzinfo=utc), verbose_name='Date du début')),
                ('type_arret', models.CharField(choices=[('A', 'arrêt de travail'), ('P', 'prolongation'), ('R', 'reprise')], max_length=1, verbose_name='Type de certifict')),
                ('duree', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='durée')),
                ('redaction_date', models.DateField(default=datetime.datetime(2017, 8, 3, 18, 28, 17, 877322, tzinfo=utc), verbose_name='délivré le')),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinic.Patient')),
            ],
        ),
        migrations.CreateModel(
            name='Certificat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('correspondant', models.CharField(blank=True, max_length=50, null=True, verbose_name='Correspondant')),
                ('certificat_date', models.DateField(default=datetime.datetime(2017, 8, 3, 18, 28, 17, 875225, tzinfo=utc), verbose_name='Date')),
                ('motif', models.CharField(blank=True, max_length=100, null=True, verbose_name='Motif')),
                ('declaration', models.CharField(blank=True, max_length=255, null=True, verbose_name='Déclare que')),
                ('closing', models.CharField(blank=True, max_length=100, null=True, verbose_name='Closing')),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinic.Patient')),
            ],
        ),
        migrations.CreateModel(
            name='Courrier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('correspondant', models.CharField(blank=True, max_length=50, null=True, verbose_name='Correspondant')),
                ('salutation', models.CharField(choices=[('C', 'Cher confrère'), ('A', 'Cher ami'), ('S', 'Chère consoeur'), ('B', 'Cher confrère, chère consoeur'), ('M', 'Cher maître')], max_length=1, verbose_name='Salutation')),
                ('courrier_date', models.DateField(default=datetime.datetime(2017, 8, 3, 18, 28, 17, 875863, tzinfo=utc), verbose_name='Date')),
                ('reponse', models.BooleanField(default=False)),
                ('diagnostic', models.CharField(blank=True, max_length=100, null=True, verbose_name='Diagnostic')),
                ('declaration', models.CharField(blank=True, max_length=255, null=True, verbose_name='Déclare que')),
                ('closing', models.CharField(blank=True, max_length=100, null=True, verbose_name='Closing')),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinic.Patient')),
            ],
        ),
        migrations.CreateModel(
            name='Ordonnance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ordonnance_date', models.DateField(default=datetime.datetime(2017, 8, 3, 18, 28, 17, 873323, tzinfo=utc), verbose_name='Date')),
                ('medoc1', models.CharField(blank=True, max_length=50, null=True)),
                ('poso1', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp1', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc2', models.CharField(blank=True, max_length=50, null=True)),
                ('poso2', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp2', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc3', models.CharField(blank=True, max_length=50, null=True)),
                ('poso3', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp3', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc4', models.CharField(blank=True, max_length=50, null=True)),
                ('poso4', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp4', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc5', models.CharField(blank=True, max_length=50, null=True)),
                ('poso5', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp5', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc6', models.CharField(blank=True, max_length=50, null=True)),
                ('poso6', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp6', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc7', models.CharField(blank=True, max_length=50, null=True)),
                ('poso7', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp7', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc8', models.CharField(blank=True, max_length=50, null=True)),
                ('poso8', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp8', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc9', models.CharField(blank=True, max_length=50, null=True)),
                ('poso9', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp9', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc10', models.CharField(blank=True, max_length=50, null=True)),
                ('poso10', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp10', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc11', models.CharField(blank=True, max_length=50, null=True)),
                ('poso11', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp11', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc12', models.CharField(blank=True, max_length=50, null=True)),
                ('poso12', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp12', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc13', models.CharField(blank=True, max_length=50, null=True)),
                ('poso13', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp13', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc14', models.CharField(blank=True, max_length=50, null=True)),
                ('poso14', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp14', models.CharField(blank=True, max_length=10, null=True)),
                ('medoc15', models.CharField(blank=True, max_length=50, null=True)),
                ('poso15', models.CharField(blank=True, max_length=25, null=True)),
                ('qsp15', models.CharField(blank=True, max_length=25, null=True)),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinic.Patient')),
            ],
        ),
        migrations.CreateModel(
            name='Stomato',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stomato_date', models.DateField(default=datetime.datetime(2017, 8, 3, 18, 28, 17, 876514, tzinfo=utc), verbose_name='Date')),
                ('diagnostic', models.CharField(blank=True, max_length=100, null=True, verbose_name='Diagnostic')),
                ('infectious_risk', models.CharField(choices=[('F', 'faible'), ('M', 'modéré'), ('I', 'important')], default='F', max_length=1, verbose_name='Risque infectieux')),
                ('thrombotic_risk', models.CharField(choices=[('F', 'faible'), ('M', 'modéré'), ('I', 'important')], default='F', max_length=1, verbose_name='Risque thrombo-embolique')),
                ('syncopal_risk', models.CharField(choices=[('F', 'faible'), ('M', 'modéré'), ('I', 'important')], default='F', max_length=1, verbose_name='Risque syncopal')),
                ('inr_cible', models.FloatField(default=1.0, verbose_name='INR Cible')),
                ('avk_interruption', models.NullBooleanField()),
                ('atb', models.NullBooleanField(default=False, verbose_name='antibioprophylaxie')),
                ('prescription', models.CharField(blank=True, max_length=255, null=True, verbose_name='Antbiothérapie')),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinic.Patient')),
            ],
        ),
    ]
