# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-04 23:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clinic', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='coroscan',
            name='calcic_total',
            field=models.PositiveSmallIntegerField(default='0', verbose_name='score Calcique global'),
        ),
        migrations.AddField(
            model_name='stress',
            name='atropin_1',
            field=models.NullBooleanField(verbose_name='Atropine 0.25 mg'),
        ),
        migrations.AddField(
            model_name='stress',
            name='atropin_2',
            field=models.NullBooleanField(verbose_name='Atropine 0.50 mg'),
        ),
        migrations.AddField(
            model_name='stress',
            name='atropin_3',
            field=models.NullBooleanField(verbose_name='Atropine 0.75 mg'),
        ),
        migrations.AddField(
            model_name='stress',
            name='atropin_4',
            field=models.NullBooleanField(verbose_name='Atropine 1.00 mg'),
        ),
        migrations.AddField(
            model_name='stress',
            name='bb_iv',
            field=models.NullBooleanField(verbose_name='Beta bloqueur IV'),
        ),
        migrations.AddField(
            model_name='stress',
            name='bb_peros',
            field=models.NullBooleanField(verbose_name='Beta bloqueur per os'),
        ),
        migrations.AddField(
            model_name='stress',
            name='dobu_10',
            field=models.NullBooleanField(verbose_name='Dobu 10 gammas'),
        ),
        migrations.AddField(
            model_name='stress',
            name='dobu_15',
            field=models.NullBooleanField(verbose_name='Dobu 15 gammas'),
        ),
        migrations.AddField(
            model_name='stress',
            name='dobu_20',
            field=models.NullBooleanField(verbose_name='Dobu 20 gammas'),
        ),
        migrations.AddField(
            model_name='stress',
            name='dobu_30',
            field=models.NullBooleanField(verbose_name='Dobu 30 gammas'),
        ),
        migrations.AddField(
            model_name='stress',
            name='dobu_40',
            field=models.NullBooleanField(verbose_name='Dobu 40 gammas'),
        ),
        migrations.AddField(
            model_name='stress',
            name='dobu_5',
            field=models.NullBooleanField(verbose_name='Dobu 5 gammas'),
        ),
        migrations.AddField(
            model_name='stress',
            name='nitrin',
            field=models.NullBooleanField(verbose_name='Dérivés nitrés'),
        ),
        migrations.AlterField(
            model_name='stress',
            name='peak_ecg',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='ECG peak dose'),
        ),
        migrations.AlterField(
            model_name='stress',
            name='recup_diastolic_bp',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='PAD récupération'),
        ),
        migrations.AlterField(
            model_name='stress',
            name='recup_ecg',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='ECG récupération'),
        ),
        migrations.AlterField(
            model_name='stress',
            name='recup_heart_rate',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='FC récupération'),
        ),
        migrations.AlterField(
            model_name='stress',
            name='recup_symptoma',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Symptômes récupération'),
        ),
        migrations.AlterField(
            model_name='stress',
            name='recup_systolic_bp',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='PAS récupération'),
        ),
    ]
