# Generated by Django 2.0.2 on 2018-02-17 08:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clinic', '0003_auto_20170812_2002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Prestation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('acte', models.CharField(max_length=50, verbose_name='acte médical')),
                ('codification', models.CharField(blank=True, max_length=6, null=True, verbose_name='codification')),
                ('tarif', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='tarif')),
            ],
        ),
        migrations.CreateModel(
            name='Reception',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(blank=True, null=True, verbose_name='Date du rendez-vous')),
                ('planned', models.NullBooleanField(default=True, verbose_name='patient programmé')),
                ('confirmed', models.NullBooleanField(default=False, verbose_name='Confirmé')),
                ('number', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='numéro du patient')),
                ('Patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rendez_vous', to='clinic.Patient')),
                ('prestation', models.ManyToManyField(blank=True, null=True, to='clinic.Prestation')),
            ],
        ),
    ]
