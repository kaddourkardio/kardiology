from django.conf.urls import url, include
from . import views
app_name = 'clinic'
urlpatterns = [
    url(r'^$', views.ListPatients.as_view(),
        name='patients'),
    url(r'(?P<pk>\d+)/$', views.DetailPatient.as_view(),
        name='detail_patient'),
    url(r'new/$', views.CreatePatient.as_view(),
        name='create_patient'),
    url(r'results/$', views.PatientSearchListView.as_view(),
        name='search'),
    url(r'patient(?P<pk1>\d+)-consultation(?P<pk2>\d+)\.pdf$',
        views.consultation_pdf, name='consultation_pdf'),
    url(r'consultations/new/$', views.CreateConsultation.as_view(),
        name='create_consultation'),
    url(r'consultations/$', views.ListConsultations.as_view(),
        name='consultations'),
]
